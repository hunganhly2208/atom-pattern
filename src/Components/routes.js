import Loadable from 'react-loadable'

import LoadingPage from './pages/LoadingPage'

// Not Found Page
export const AsyncNotFound = Loadable({
  loader: () => import('./pages/NotFound'),
  loading: LoadingPage,
})

// User management page
export const AsyncUser = Loadable({
  loader: () => import('Containers/User'),
  loading: LoadingPage,
})
