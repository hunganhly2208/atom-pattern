import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import * as routes from './routes'

const App = () => (
  <Switch>
    <Redirect exact from="/" to="/user" />
    <Route path="/user" exact component={routes.AsyncUser} />
    <Route component={routes.AsyncNotFound} />
  </Switch>
)

export default App
