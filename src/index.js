import React from 'react'
import ReactDOM from 'react-dom'
import { createHashHistory } from 'history'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router/immutable'

import createStore from './Stores/CreateStore.js'
import { unregister } from './registerServiceWorker'
import App from 'Components/App'
import './i18n'

const history = createHashHistory()

async function init() {
  const store = createStore(history)

  const render = () =>
    ReactDOM.render(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </Provider>,
      document.getElementById('root')
    )
  render()
}

init()

unregister()
